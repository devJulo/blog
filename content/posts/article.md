---
title: "Article"
date: 2021-04-04T07:53:52Z
draft: false
---

2021 Je lance mon premier blog, ça commence bien mdr...

J'ai eu cette idée il n'y a pas si longtemps et je suis pas encore tout à fait sûr de sa pertinance. Mais bon, je me suis dis qu'au pire ça serait marrant comme expérience et puis j'ai peut-être deux trois truc intéréssant à partager. En faite, l'idée derrière ce blog c'est plus de me créer un espace ou je peux noter des choses pour plus tard. Vous savez, vous avez une idée, elle vous semble pertinente, vous commencez à creuser le truc et puis... Vous oubliez, ça part aux oubliettes pendant 1an et un beau jour... ça revient comme ça, vous savez pas trop pour quoi mais vous devez refaire tout la démarche de pratiquement zéro. Et bien ce blog aura pour but de me facilité ce "retour" sur des sujets auquels je me suis intéréssé (et qui m'intéresse probablement toujours) mais que je n'ai pas forcement le temps de creuser plus en profondeur. 

La bise.
